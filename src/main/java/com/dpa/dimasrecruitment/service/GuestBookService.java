package com.dpa.dimasrecruitment.service;

import com.dpa.dimasrecruitment.model.GuestBook;
import com.dpa.dimasrecruitment.repository.GuestBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestBookService {

    @Autowired
    GuestBookRepository guestBookRepository;

    public List<GuestBook> getSearchBy(List<String> searchBy, List<Object> keywords, String orderBy, String orderType) {
        return guestBookRepository.getSearchBy(GuestBook.class, searchBy, keywords, orderBy, orderType);
    }

    public void saveOrUpdate(GuestBook guestBook) throws Exception {
        guestBookRepository.saveOrUpdate(guestBook);
    }

    public List<GuestBook> getAll() throws Exception{
        return guestBookRepository.getAll(GuestBook.class);
    }

    public void delete(int id) {
        guestBookRepository.delete(GuestBook.class, id);
    }
}
