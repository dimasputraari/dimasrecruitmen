package com.dpa.dimasrecruitment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DimasrecruitmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(DimasrecruitmentApplication.class, args);
    }

}
