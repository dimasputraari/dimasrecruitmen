package com.dpa.dimasrecruitment.controller;

import com.dpa.dimasrecruitment.model.GuestBook;
import com.dpa.dimasrecruitment.service.GuestBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/guestbook")
public class GuestBookController {

    @Autowired
    private SimpleController simpleController;

    @Value("${spring.application.name}")
    String appName;

    @Autowired
    private GuestBookService guestBookService;

    @GetMapping("/getall")
    public List<GuestBook> getList() {
        try {
            return guestBookService.getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
