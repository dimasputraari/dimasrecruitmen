package com.dpa.dimasrecruitment.controller;

import com.dpa.dimasrecruitment.model.GuestBook;
import com.dpa.dimasrecruitment.service.GuestBookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@CrossOrigin(origins = "*")
public class SimpleController {

    private String oldJabatan;

    @Autowired
    private GuestBookService guestBookService;

    @Value("${spring.application.name}")
    String appName;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("tittle", "Home");
        model.addAttribute("appName", appName);
        return "home";
    }

    @GetMapping("/promotedemote")
    public String promoteDemote(Model model) {
        model.addAttribute("tittle", "guest book");
        model.addAttribute("appName", appName);
        return "promoteDemote";
    }

    /*
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id, Model model) {
        model.addAttribute("tittle", "artists");
        model.addAttribute("appName", appName);
        artistsService.delete(id);
        return "promoteDemote";
    }
    */

    /*
    @GetMapping("/form/{id}")
    public String form(@PathVariable("id") int id, Model model) {
        model.addAttribute("tittle", "Promote / Demote Form");
        model.addAttribute("appName", appName);
        Artists artist = artistsService.getById(id);
        model.addAttribute("artist", artist);
        return "form";
    }
    */

    @GetMapping("/form")
    public String form(Model model) {
        model.addAttribute("tittle", "Artists Form");
        model.addAttribute("appName", appName);
        model.addAttribute("guestBook", new GuestBook());
        return "form";
    }

    @PostMapping("/save")
    public String save(@Valid GuestBook guestBook, BindingResult result, Model m) {
        try {
            guestBookService.saveOrUpdate(guestBook);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "promoteDemote";
    }

    /*
    @PostMapping("/testJson")
    public ResponseEntity<Map<String, Object>> testJson(@RequestBody Map<String, Object> request) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseResult = new HashMap<>();
        Karyawan karyawan = mapper.convertValue(request.get("isi"),Karyawan.class);
        responseResult.put("nik", karyawan.getNik());
        responseResult.put("name", karyawan.getName());
        return new ResponseEntity<>(responseResult, HttpStatus.OK);
    }
    */
}
