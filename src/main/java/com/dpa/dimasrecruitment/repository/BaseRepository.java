package com.dpa.dimasrecruitment.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class BaseRepository<T> {

    public static final String ACTIVE = "active";
    public static final String SEQUENCE = "sequenceNumber";

    @PersistenceContext
    protected EntityManager em;

    public <T> List<T> getSearchBy(Class<T> clazz, List<String> searchBy, List<Object> keywords,
                                   String orderBy, String orderType) {
        try {
            StringBuffer query = new StringBuffer("SELECT ");
            query.append("x FROM " + clazz.getName() + " x ");
            query.append("WHERE 1=1 ");
            for (int i = 0; i < searchBy.size(); i++) {
                if (!searchBy.get(i).equals("") && !keywords.get(i).equals("")) {
                    if (searchBy.get(i).equals(ACTIVE)) {
                        query.append(" AND x." + searchBy.get(i) + " = ?" + i);
                    } else {
                        query.append(" AND LOWER(x." + searchBy.get(i) + ") like ?" + i);
                    }
                }
            }
            if (!orderBy.equals("")) {
                query.append(" ORDER BY x." + orderBy + " ");
            } else {
                query.append(" ORDER BY x." + SEQUENCE + " ");
            }

            if (!orderType.equals("")) {
                query.append(orderType);
            } else {
                query.append(" ASC");
            }

            Query que = em.createQuery(query.toString());

            for (int i = 0; i < searchBy.size(); i++) {
                if (!searchBy.get(i).equals("") && !keywords.get(i).equals("")) {
                    if (searchBy.get(i).equals(ACTIVE)) {
                        que.setParameter(i, Integer.parseInt((String) keywords.get(i)));
                    } else {
                        que.setParameter(i, "%" + ((String) keywords.get(i)).toLowerCase() + "%");
                    }
                }
            }

            return que.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveOrUpdate(T t) throws Exception {
        this.em.merge(t);
    }

    public List<T> getAll(Class<T> clazz) throws Exception {
        return em.createQuery("SELECT c FROM "+ clazz.getName() +" c").getResultList();
    }

    public T getById(Class<T> clazz, int id) {
        return em.find(clazz, id);
    }

    public void delete(Class<T> clazz, int id) {
        em.remove(getById(clazz, id));
    }
}
