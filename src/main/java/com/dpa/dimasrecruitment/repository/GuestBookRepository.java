package com.dpa.dimasrecruitment.repository;

import com.dpa.dimasrecruitment.model.GuestBook;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GuestBookRepository extends BaseRepository{
    public List<GuestBook> getSearchBy(List<String> searchBy, List<Object> keywords, String orderBy, String orderType) {
        return super.getSearchBy(GuestBook.class, searchBy, keywords, orderBy, orderType);
    }

    public void saveOrUpdate(GuestBook guestBook) throws Exception {
        super.saveOrUpdate(guestBook);
    }

    public List<GuestBook> getAll() throws Exception{
        return super.getAll(GuestBook.class);
    }

    public void delete(int id) {
        super.delete(GuestBook.class, id);
    }
}
